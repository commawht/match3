﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MStructs;
using MClasses;
//using ZeeMatrix;

public class Board :MonoBehaviour
{
    //info for building
    public int nrOfRows;                  //number of rows/columns in the board-- for now we make it a square board

    private ColorAndWeight[] _thisBoardsAvailablePieces;   //pieces that can be used to generate this board

    public ColorAndWeight[] thisBoardsAvailablePieces
    {
        get { return _thisBoardsAvailablePieces; }
        set {
            Debug.Log("new values for this board's available pieces");
            _thisBoardsAvailablePieces = value; }
    }
    
    private float boardDimension;           //the dimension in world units of the board

    public float cellDimension;            //dimension in world units of the cell.  used as a multiplyer for scale since base size is 1m 
    public Vector3 cellPositionOffset;     // offset aplied to all cells to center the board around origin

    //private float boardSize;

    

    //public static ABoard currentBoard;

    //info about the generated board
    //public Transform[,] board; // the cells inside the board
    //private int[,] ids; //FUGLY perhaps keep only ids ? perhaps a struct or class?

    public Cell[,] cellMatrix;

    public float cellFramingDistance;  //distance from cell at 
    public float boardFramingDistance;
    public int boardDepth = 0; //the depth at witch this board resides
    public Cell parentCell; //the cell that is a parent to this board


    private MPos[] _exits;
    private MPos[] exitsSides ={//clockwise exits for a square grid (y up, x right, origin bottom left)
                                           new MPos( 0, 1),
                                           new MPos( 1, 0),
                                           new MPos( 0,-1),
                                           new MPos(-1, 0)
                                        };
    private MPos[] exitsSidesAndD ={    new MPos( 0, 1),
                                        new MPos( 1, 1),
                                        new MPos( 1, 0),
                                        new MPos( 1, -1),
                                        new MPos( 0, -1),
                                        new MPos( -1, -1),
                                        new MPos( -1, 0),
                                        new MPos( -1, 1),
                                 };


    void Start(){

        //Initialize();   //TEMP

    }

    public void Initialize(Cell parentCell = null)
    {
        //_piecesWeights = piecesWeights;//??
        boardDimension = this.transform.lossyScale.x; // make dimension relative to parent scale
        cellDimension = boardDimension / nrOfRows;  //position and scale multiplier so we can instantiate inside a cell

        cellMatrix = new Cell[nrOfRows, nrOfRows];

        // what exits to use
        //exits = exitsSides;             //exits for sides of square
        _exits = exitsSidesAndD;       // exits for sides of square and corners
                                        //exits for hexagon

        float centerOfset = boardDimension / 2 - cellDimension/2; // ofset 000 to get teh board center in 000
        cellPositionOffset = new Vector3(-centerOfset, -centerOfset, 0);
        //Debug.Log(" " + cellPositionOffset);
        float cellDiagonal = Helpers.CalcBoxDiagonal(width: cellDimension,
                                                        depth: cellDimension,
                                                        height: cellDimension);
        float boardDiagonal =  Helpers.CalcBoxDiagonal(width: boardDimension,
                                                        depth: cellDimension,
                                                        height: boardDimension);
        cellFramingDistance = Camera.main.CalculateSFramingDistance(cellDiagonal/2);
        boardFramingDistance = Camera.main.CalculateSFramingDistance(boardDiagonal / 2);
        NewBoard(parentCell);
        


        Debug.Log("Initialize ended @ " + Time.time);
    }

 

    //public void NewCell(int x, int y)
    //{
    //    Cell cellScript;
    //    //get a random piece
    //    //int randPiece = Random.Range(0, _availablePieces.Length);
    //    int randPiece = Helpers.WeightedRandom(_piecesWeights);
    //    //instantiate piece
    //    Vector3 cellPosition = new Vector3(x, y, 0) * cellDimension + cellPositionOffset + this.transform.position;

    //    Transform obj = Instantiate(availablePieces[randPiece], cellPosition, Quaternion.identity) as Transform;
    //    obj.localScale = new Vector3(cellDimension, cellDimension, cellDimension);
    //    obj.parent = this.transform;

    //    //write piece in board
    //    board[x, y] = obj; //write the transform
    //    ids[x, y] = randPiece;  //write the id

    //    cellScript = board[x, y].gameObject.GetComponent<Cell>();
    //    if (cellScript != null)
    //    {
    //        cellScript.parentBoard = this;
    //        cellScript._posInBoard = new MPos(x, y);
    //        cellScript.cellFramingDistance = cellFramingDistance;
    //        cellScript.parentBoardFramingDistance = boardFramingDistance;
    //        cellScript.Initialize();
    //    }
    //    else
    //    {
    //        Debug.Log("cell prefab doesn't have teh script ACell");
    //    }

    //}
    public Cell NewCell(MPos position, Transform geometry, int id)
    {
        Cell cellScript;

        
        //instantiate piece
        Vector3 cellPosition = new Vector3(position.x, position.y, 0) * this.cellDimension + cellPositionOffset + this.transform.position;
        Transform obj = Instantiate(geometry, cellPosition, Quaternion.identity) as Transform;
        obj.name = "Cell";
        obj.localScale = new Vector3(1f, 1f, 1f) * this.cellDimension;
        cellScript = obj.gameObject.AddComponent<Cell>();
        cellScript.parentBoard = this;
        //Debug.Log("seting position " + position);
        cellScript.posInBoard = position;
        //Debug.Log("seting id " + id);

        cellScript.myID = id;
        cellScript.transform.parent = this.transform;

        cellScript.Initialize();
        return cellScript;

    }
    public void NewBoard(Cell parentCell = null)
    {

        //Debug.Log("NewBoard started @ " +Time.time);
        
        for (int x = 0; x < nrOfRows; x++) // for each row
            for (int y = 0; y < nrOfRows; y++)//for each column
            {

                NewCell(position: new MPos(x,y),
                        geometry: GameMaster.gmInstance.baseGeometry,
                        id: Helpers.WeightedRandom(this.thisBoardsAvailablePieces)
                        );

            }
        //CellCount(this);//TEST
        //Debug.Log("NewBoard ended @ " + Time.time);
    }

    public IEnumerator Selected(MPos[] selection)
    {

        //check if selected are neighbours
        if (NeighbourCheck(selection, _exits)){
            yield return StartCoroutine(Swap(selection));
            if (Match3(selection, _exits))
            {
                Debug.Log("we have a match 3");
            }
            else
            {
                Debug.Log("we don't have a match 3");
                yield return StartCoroutine(Swap(selection));
            }

        }

    }

    public IEnumerator Swap(MPos[] selection)
    {
        /*swap in board
        swap in ids
        inform pieces
        move pieces
        */

        //Debug.Log("we are going to swap");

        MPos first = selection[0];
        MPos second = selection[1];
        //Swap transforms

        Cell tempCell = cellMatrix[first.x, first.y]; //
        cellMatrix[second.x, second.y].posInBoard = first;
        tempCell.posInBoard = second;
        DebugInfos.TestBoard(this);
       
        Cell firstScript = cellMatrix[first.x, first.y];
        Cell secondScript = cellMatrix[second.x, second.y];

        Vector3 tempPositionF = cellMatrix[first.x, first.y].transform.position;
        Vector3 tempPositionS = cellMatrix[second.x, second.y].transform.position;

        IEnumerator firstE = firstScript.MoveTo(tempPositionS);
        IEnumerator secondE = secondScript.MoveTo(tempPositionF);

      while (firstE.MoveNext() && secondE.MoveNext())
      {
          yield return null;
      }



    }

    public bool Match3(MPos[] selections, MPos[] exits) 
    {
        List<MPos> validExitsList = new List<MPos>();                         //list of exits for each line .. usefull?
        List<List<MPos>> inlineNeighbours = new List<List<MPos>>();           //list of groups with > 3 in a row
        List<MPos> tempNeighbour;
        Debug.Log("let's test " + exits.Length + "exits");
        foreach (MPos pos in selections)
        {  //go through each selection
            //Debug.Log("selection is "+ pos );
            for (int i = 0; i < exits.Length / 2; i++) //go through half the exits
            {
                Debug.Log("exit nr " + i);
                MPos exit = exits[i];
                MPos opositeExit = exits[i + (exits.Length / 2)];

                MPos current = pos; // starting position for check
                bool forward = true;
                bool backward = true;
                tempNeighbour = new List<MPos>() { pos }; //make first element in list the starting position


                //TODO perhaps refactor this as we have duplicate code in the 2 whiles
                while (forward)//check  in one direction
                {
                    Debug.Log("Cechking Forward");
                    current += exit; // move one cell in the same direction
                    //int curX = (int)current.x;
                    //int curY = (int)current.y;
                    //TODO big nono expensive, don't be lazy
                    try
                    {
                        if (cellMatrix[pos.x, pos.y].myID == cellMatrix[current.x, current.y].myID)
                        {
                            tempNeighbour.Add(current);
                        }
                        else forward = false;
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        Debug.Log("they went too far.But it's ok");
                        forward = false;
                        continue;//??
                    }
                }
                current = pos;
                while (backward)//check in the other direction
                {
                    Debug.Log("Cecking Backward");
                    current += opositeExit; // move one cell in the same direction
                    try
                    {
                        if (cellMatrix[pos.x, pos.y].myID == cellMatrix[current.x, current.y].myID)
                        {
                            //on current exit we have a neighbour with same id

                            tempNeighbour.Insert(0, current);
                        }
                        else backward = false;
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        Debug.Log("they went too far.But it's ok");
                        backward = false;
                        continue;//??
                    }

                }
                Debug.Log(" neighbours in a row: " + tempNeighbour.Count);
                if (tempNeighbour.Count > 2)
                {
                    inlineNeighbours.Add(tempNeighbour);
                    validExitsList.Add(exit);
                }
            }
        }
        Debug.Log("inline neighbours " + inlineNeighbours.Count);
        if (inlineNeighbours.Count > 0)
        {
            KillemAll(inlineNeighbours);
            return true;

        }
        else return false;
    }

    public void KillemAll(List<List<MPos>> killListList)
    {
        HashSet<MPos> markedForDeath = new HashSet<MPos>();
        foreach (List<MPos> killList in killListList)
        {
            foreach (MPos kill in killList)
            {

                //perhaps an object pool would be nicer?
                //board[kill.x, kill.y].transform.localScale = board[kill.x, kill.y].transform.localScale * 0.5f;
                //Destroy(board[kill.x, kill.y].gameObject);
  
                if (markedForDeath.Add(kill))
                {
                    //Debug.Log("removed a duplicate");
                }

                //Debug.Log("hash has " + markedForDeath.Count);
            }
        }
        //Debug.Log("hash has " + markedForDeath.Count);

        foreach (var killed in markedForDeath)
        {
            //Destroy(board[killed.x, killed.y].gameObject);
            StartCoroutine(cellMatrix[killed.x, killed.y].Die());

            //board[killed.x, killed.y] = null;
            //ids[killed.x, killed.y] = 9001; //it's over 9000 TODO find another way
            
        }
    }

    private bool NeighbourCheck(MPos[] selection, MPos[] exits)
    {

        for (int i = 0; i < exits.Length; i++ )
        {
            //checks if first selection is an exit of second selection or viceversa
            if (exits[i] + selection[0] == selection[1] ||
                selection[0] == exits[i] + selection[1])
            {
                return true;
            }
        }
        return false;
    }

    //public void Mtest()
    //{
    //    for (int i = 0; i < cellMatrix.GetLength(0); i++)
    //    {
    //        for (int j = 0; j < cellMatrix.GetLength(1); j++)
    //        {
    //            if (cellMatrix[i, j].posInBoard != new MPos(i, j))
    //            {
    //                Debug.Log("Shieeeet cell is: " + cellMatrix[i, j].posInBoard + "board matrix is " + new MPos(i, j));
    //            }
    //            else { Debug.Log("yaaaiii"); }
    //        }
    //    }
    //}

}




        



