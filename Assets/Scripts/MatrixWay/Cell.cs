﻿using UnityEngine;
using System.Collections;
using MStructs;
//using ZeeMatrix;

public class Cell : MonoBehaviour {
    //my info
    int scoreValue = 1;




    private Board _parentBoard=null;
    public Board parentBoard
    {
        get 
        { 
            return _parentBoard; 
        }
        set
        {
            _parentBoard = value;
            this.transform.parent = value.transform.parent; // parent this cell to the parentboard
        }
    }

    private MPos _posInBoard;     //TEST custom struct for keeping position in matrix in a int vector2
    public MPos posInBoard
    {
        get
        {
            return _posInBoard;
        }
        set
        {
            //Debug.Log("current pos in board is " + _posInBoard);
            //Debug.Log("setting pos in board at " + value);
            if (parentBoard != null)
            {

                if (parentBoard.cellMatrix != null)
                {
                    //if (parentBoard.cellMatrix[posInBoard.x, posInBoard.y] == null)
                    //{
                        _posInBoard = value;
                        parentBoard.cellMatrix[_posInBoard.x, _posInBoard.y] = this;
                    
                    //}
                    //else
                    //{
                    //    Debug.Log("the cell can't move to this position because it is not empty SELFDESTROY _" + parentBoard.cellMatrix[posInBoard.x, posInBoard.y].posInBoard);
                    //    Destroy(this.gameObject);
                    //}

                }
                else
                {
                    Debug.Log("cell matrix is null");
                    
                }

            }
            else
            {
                Debug.Log("Tried to set pos in board and failed because there is no board in parentBoard");
            }
        }
    }

    private int _myID;
    public int myID
    {
        get
        {
            return _myID;
        }
        set
        {
            if (this.gameObject.renderer.material )
            {
                if (value < GameMaster.gmInstance.lvlPieces.Length && value >= 0)
                {
                    _myID = value;
                    this.gameObject.renderer.material.color = GameMaster.gmInstance.lvlPieces[value].color;
                    //change it in the board matrix also
                }
                else {
                Debug.Log("Trying to set the value: " + value
                    + " and it is bad (" + value + " <" + parentBoard.thisBoardsAvailablePieces.Length + "  && value >= 0)");
                this.ShowInfo(parentBoard: true);
                }
                
            }
            else Debug.Log("trying to set the id of a cell and failed because  the cell doesn't have a material");

        }
    }

    public Board childBoard = null;
    private bool inverted = false; // arethe normals inverted on this object's geometry? (inverted when zoomed in)

    private float distanceToCamera;
    //public float cellFramingDistance;
    //public float parentBoardFramingDistance;


    private static Transform firstSelection;
    private static Transform secondSelection;

 
	void Start () {
        if (!this.transform.gameObject.GetComponent<MeshCollider>())
        {
            transform.gameObject.AddComponent<MeshCollider>();
        }

}
	

    void OnMouseDown() {
        //selection of the 2 cells
        if (firstSelection == null)
        {
            firstSelection = this.transform;
            
            secondSelection = null;//pbly not necessary
        }
        else
            if (firstSelection == this.transform)
            {
                firstSelection = null;
            }
            else {
                secondSelection = this.transform;
                //Debug.Log("first selection is @" + firstSelection.GetComponent<Cell>()._posInBoard);
                //Debug.Log("second selection is @ " + secondSelection.GetComponent<Cell>()._posInBoard);
                GameMaster.moves += 1;
                 StartCoroutine(_parentBoard.Selected(new MPos[] {firstSelection.GetComponent<Cell>()._posInBoard, _posInBoard}));

                //reset selections
                firstSelection = null;
                secondSelection = null;
            }


    }

    void OnMouseOver()
    {
        //myID = Helpers.WeightedRandom(GameMaster.piecesWeights);
        //zoom in 
 
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {

            //zooming
            if (!Camera.main.Dolly(this.transform.position, Input.GetAxis("Mouse ScrollWheel"), 0.5f, _parentBoard.cellDimension / 2 + 0.01f))
            {
                //meh empty bodyed if not so cool
                Debug.Log("we can't go deeper - reason: near clipping");
            }
                distanceToCamera = Vector3.Distance(this.transform.position, Camera.main.transform.position);
            // zoom/dolly towards this cell
                if (distanceToCamera < parentBoard.cellFramingDistance)
                { 

                    if (childBoard == null)
                    {
                        //Debug.Log("we are going to generate a new board here");
                        childBoard = GameMaster.gmInstance.NewBoardInCell(this);
                        childBoard.parentCell = this;
                    }
                    else
                    {
                        //Debug.Log("already has a child board");
                    }
                    GameMaster.currentBoard = childBoard;
                    if (inverted == false)
                    {
                        Helpers.ReverseNormals(this.gameObject.GetComponent<MeshFilter>());
                        transform.GetComponent<MeshCollider>().sharedMesh = this.gameObject.GetComponent<MeshFilter>().mesh;
                        inverted = true;

                    }
                    Camera.main.FrameBoard(GameMaster.currentBoard);
                    //this.gameObject.collider.enabled = false;
                    //this.gameObject.renderer.enabled = false;

                }
            // zoom/dolly away from this cell
            if (distanceToCamera > parentBoard.boardFramingDistance)
            {
                //Debug.Log("zooming out");
                if( this._parentBoard.parentCell){
                        if (_parentBoard.parentCell.inverted == true)
                        {
                            Debug.Log("its inverted so invert the inversion");
                            Helpers.ReverseNormals(_parentBoard.parentCell.gameObject.GetComponent<MeshFilter>());
                            _parentBoard.parentCell.transform.GetComponent<MeshCollider>().sharedMesh = this.gameObject.GetComponent<MeshFilter>().mesh;
                            //Helpers.ReverseNormals(this.gameObject.GetComponent<MeshFilter>());
                            _parentBoard.parentCell.inverted = false;
                            GameMaster.currentBoard = _parentBoard.parentCell.parentBoard;
                            Camera.main.FrameCell(this.parentBoard.parentCell);
                        }

                        //this.transform.parent.parent.gameObject.collider.enabled = true; //FUGLY and dependent on parenting 
                        //this.transform.parent.parent.gameObject.renderer.enabled = true;
                        //Debug.Log("we are going up a level");
                    
                    
                }
                else
                {
                    Debug.Log("no parent to zoom to");
                }
            }


        }
    }



    public void Initialize(){
        StartCoroutine(EmergeAnim());
    }
    public void Inform(Board board)
    {

        _parentBoard = board ;
 
    }

    public void Inform(MPos posInM)
    {
        //Debug.Log("Inform old pos: " + posInBoard + " new pos: " + posInM);
        _posInBoard = posInM;
    }

    public IEnumerator  Die()
    {
        //animate death
        //Debug.Log("oh noes im gona die");
        yield return StartCoroutine(DestroyAnim()) ;
        parentBoard.NewCell(   position: this.posInBoard,
                                geometry: GameMaster.gmInstance.baseGeometry,
                                id: Helpers.WeightedRandom(parentBoard.thisBoardsAvailablePieces)
                                );

        Destroy(this.gameObject);
    }

    private IEnumerator DestroyAnim()
    {
        float duration = 0.5f; //TODO expose duration - the duration in second for the animation(lerp)
        float startTime = Time.time;
        Vector3 scale = this.transform.localScale;
        while (Time.time < startTime + duration)
        {

            this.transform.localScale = Vector3.Lerp(scale, new Vector3(0f,0f,0f), (Time.time - startTime) / duration);

            yield return null;
        }

    }
    private IEnumerator EmergeAnim()
    {
        float duration = 0.5f; //TODO expose duration - the duration in second for the animation(lerp)
        float startTime = Time.time;
        Vector3 scale = this.transform.localScale;
        while (Time.time < startTime + duration)
        {

            this.transform.localScale = Vector3.Lerp(new Vector3(0f, 0f, 0f), scale, (Time.time - startTime) / duration);

            yield return null;
        }
        this.transform.localScale=scale;

    }

   public IEnumerator MoveTo(Vector3 newPos) 
    {
       //TODO use curves to easein/out movement?

        //Debug.Log("moving from" +this.transform.position+ " to " + newPos);
        float duration = 0.5f; //TODO expose duration - the duration in second for the animation(lerp)
        float startTime = Time.time;
        Vector3 startPos = this.transform.position;
        while (Time.time <startTime + duration)
        {
        
                this.transform.position = Vector3.Lerp(startPos, newPos, (Time.time - startTime) / duration);

            yield return null;
        }
       this.transform.position = newPos;
       
        
    }

   public void OnDestroy()
   {
       //Debug.Log("bang bang " + this.gameObject + "is dead");
       GameMaster.score += scoreValue;

   }


}
