﻿using UnityEngine;
using System;
using System.Collections;
using MClasses;
using MStructs;

public static class DebugInfos  {

    public static void TestBoard(Board board)
    {
        for (int i = 0; i < board.cellMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < board.cellMatrix.GetLength(1); j++)
            {
                if (board.cellMatrix[i, j].posInBoard != new MPos(i, j))
                {
                    throw new Exception("Shieeeet cell is: " + board.cellMatrix[i, j].posInBoard + "board matrix is " + new MPos(i, j));
                  
                }
                //else { Debug.Log("yaaaiii"); }
            }
        }
    }

    public static void ShowBoardInfo(this Board board, bool availablePieces = false)
    {
        if (availablePieces ){
            Debug.Log(board.gameObject.name + " haas " + board.thisBoardsAvailablePieces.Length + " pieces available to build it");
        }
    }
    public static void ShowInfo(this Cell cell, bool parentBoard = false)
    {
        if (parentBoard)
        {
            Debug.Log("This cell has " + cell.parentBoard.gameObject.name + "as parent");
        }
    }


    public static void PrintWeights(ColorAndWeight[] colWeightArray)
    {
        for (int i = 0; i < colWeightArray.Length; i++)
        {
            Debug.Log("weight at index " + i + " is " + colWeightArray[i].weight);
        }
    }
}
