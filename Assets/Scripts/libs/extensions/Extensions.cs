﻿using UnityEngine;
using System;
using System.Collections;

public static class CameraExtensions {
    /// <summary>
    /// Calculates the distance the camera has to be from the object to have the whole object in frame
    /// </summary>
    /// <param name="camera"> the camera to take the field of view from</param>
    /// <returns>float </returns>
    public static float CalculateFramingDistance(this Camera camera, Bounds bounds)
    {
        Vector3 max = bounds.size;
        // Get the radius of a sphere circumscribing the bounds
        float radius = max.magnitude / 2f;
        // Get the horizontal FOV, since it may be the limiting of the two FOVs to properly encapsulate the objects
        float horizontalFOV = 2f * Mathf.Atan(Mathf.Tan(camera.fieldOfView * Mathf.Deg2Rad / 2f) * camera.aspect) * Mathf.Rad2Deg;
        // Use the smaller FOV as it limits what would get cut off by the frustum        
        float fov = Mathf.Min(camera.fieldOfView, horizontalFOV);
        float dist = radius / (Mathf.Sin(fov * Mathf.Deg2Rad / 2f)); //RESEARCH why not just radius? inscribing?
        //Debug.Log("Radius = " + radius + " dist = " + dist);
        return dist;
    }
    /// <summary>
    /// Calculates the distance the camera has to be from the object to have the whole object in frame
    /// </summary>
    /// <param name="camera"> the camera to take the field of view from</param>
    /// <returns>float </returns>
    public static float CalculateSFramingDistance(this Camera camera, float radius)
    {

        //// Get the radius of a sphere circumscribing the bounds
        // radius = max.magnitude / 2f;

        // Get the horizontal FOV, since it may be the limiting of the two FOVs to properly encapsulate the objects
        float horizontalFOV = 2f * Mathf.Atan(Mathf.Tan(camera.fieldOfView * Mathf.Deg2Rad / 2f) * camera.aspect) * Mathf.Rad2Deg;
        // Use the smaller FOV as it limits what would get cut off by the frustum        
        float fov = Mathf.Min(camera.fieldOfView, horizontalFOV);
        float dist = radius / (Mathf.Sin(fov * Mathf.Deg2Rad / 2f)); //RESEARCH why not just radius? inscribing?
        //Debug.Log("Radius = " + radius + " dist = " + dist);
        return dist;
    }
    /// <summary>
    /// Dolly("zoom") towards the target without movingthe target to the center in percent? of distance between cam and target
    /// </summary>
    /// <param name="target">point in space to dolly to</param>
    /// <param name="inputMovement">delta driving the movement (mouse scroll wheel, distance between touches for pinch) </param>
    /// <param name="step">0 no movement, 1 move the entire distance in one step</param>
    public static void Dolly(this Camera camera, Vector3 target, float inputMovement, float step)
    {
        //Debug.Log("dolly-ing");
        float distanceToTarget;
        Quaternion initialRotation;
        Quaternion targetRotation;
        Vector3 movement;

        initialRotation = camera.transform.rotation;
        targetRotation = Quaternion.LookRotation(target - camera.transform.position); //TODO why diference is needed?
        distanceToTarget = Vector3.Distance(camera.transform.position, target);
        //Debug.Log("distanceToTarget is: " + distanceToTarget);
        distanceToTarget = distanceToTarget - inputMovement * step * distanceToTarget;
        movement = new Vector3(0.0f, 0.0f, -distanceToTarget);

        Vector3 pos = targetRotation * movement + target;

        camera.transform.rotation = initialRotation;
        camera.transform.position = pos;

    }
    public static bool Dolly(this Camera camera, Vector3 target, float inputMovement, float step, float clipOffset)
    {
        if (    camera.nearClipPlane + clipOffset >= Vector3.Distance(camera.transform.position, target)
                && inputMovement > 0)
        {
            return false;
        }
 
            Dolly(camera, target, inputMovement, step);
            return true;
  
        
    }
    /// <summary>
    /// function that looks at origin of object and frames it and all children in the camera
    /// <remarks> Based on: answered May 24, 2012 at 10:39 PM Amir Ebrahimi  ("http://answers.unity3d.com/questions/13267/how-can-i-mimic-the-frame-selected-f-camera-move-z.html") </remarks>
    /// </summary>
    /// <param name="go">Game Object to look at</param>
    public static void FrameSelection(this Camera camera, Bounds b)
    {
        Vector3 max = b.size;
        // Get the radius of a sphere circumscribing the bounds
        float radius = max.magnitude / 2f;//RESEARCh is max magnitude of size of bound /2 = diagonal/2? 
        // Get the horizontal FOV, since it may be the limiting of the two FOVs to properly encapsulate the objects
        float horizontalFOV = 2f * Mathf.Atan(Mathf.Tan(camera.fieldOfView * Mathf.Deg2Rad / 2f) * camera.aspect) * Mathf.Rad2Deg;
        // Use the smaller FOV as it limits what would get cut off by the frustum        
        float fov = Mathf.Min(camera.fieldOfView, horizontalFOV);
        float dist = radius / (Mathf.Sin(fov * Mathf.Deg2Rad / 2f)); //RESEARCH why not just radius? inscribing?
        //Debug.Log("Radius = " + radius + " dist = " + dist);

        camera.transform.position = new Vector3(camera.transform.position.x, camera.transform.position.y, -dist); //RESEARCH  original was with +dist, but that rotates the camera 180 in m3 case
        if (camera.orthographic)
            camera.orthographicSize = radius;

        // Frame the object hierarchy
        camera.transform.LookAt(b.center);
    }

    /// <summary>
    /// function that moves the camera to origin of object and frames it and all it's children in the camera
    /// <remarks> Based on: answered May 24, 2012 at 10:39 PM Amir Ebrahimi  ("http://answers.unity3d.com/questions/13267/how-can-i-mimic-the-frame-selected-f-camera-move-z.html") </remarks>
    /// </summary>
    /// <param name="go">Game Object to look at</param>
    public static void FrameSelectionM(this Camera camera, Bounds b)
    {
        //Vector3 max = b.size;
        //// Get the radius of a sphere circumscribing the bounds
        //float radius = max.magnitude / 2f;
        //// Get the horizontal FOV, since it may be the limiting of the two FOVs to properly encapsulate the objects
        //float horizontalFOV = 2f * Mathf.Atan(Mathf.Tan(camera.fieldOfView * Mathf.Deg2Rad / 2f) * camera.aspect) * Mathf.Rad2Deg;
        //// Use the smaller FOV as it limits what would get cut off by the frustum        
        //float fov = Mathf.Min(camera.fieldOfView, horizontalFOV);
        //float dist = radius / (Mathf.Sin(fov * Mathf.Deg2Rad / 2f)); //RESEARCH why not just radius? inscribing?
        ////Debug.Log("Radius = " + radius + " dist = " + dist);
        float dist = camera.CalculateFramingDistance(b);
        camera.transform.position = new Vector3(b.center.x, b.center.y, -dist); //RESEARCH  original was with +dist, but that rotates the camera 180 in m3 case
    //    if (camera.orthographic)
    //        camera.orthographicSize = radius;


    }

    /// <summary>
    /// function that moves the camera to origin of object and frames it and all it's children in the camera
    /// <remarks> Based on: answered May 24, 2012 at 10:39 PM Amir Ebrahimi  ("http://answers.unity3d.com/questions/13267/how-can-i-mimic-the-frame-selected-f-camera-move-z.html") </remarks>
    /// </summary>
    /// <param name="go">Game Object to look at</param>
    public static void FrameSphere(this Camera camera, Vector3 center, float radius)
    {
        //Vector3 max = b.size;
        //// Get the radius of a sphere circumscribing the bounds
        //float radius = max.magnitude / 2f;
        //// Get the horizontal FOV, since it may be the limiting of the two FOVs to properly encapsulate the objects
        //float horizontalFOV = 2f * Mathf.Atan(Mathf.Tan(camera.fieldOfView * Mathf.Deg2Rad / 2f) * camera.aspect) * Mathf.Rad2Deg;
        //// Use the smaller FOV as it limits what would get cut off by the frustum        
        //float fov = Mathf.Min(camera.fieldOfView, horizontalFOV);
        //float dist = radius / (Mathf.Sin(fov * Mathf.Deg2Rad / 2f)); //RESEARCH why not just radius? inscribing?
        ////Debug.Log("Radius = " + radius + " dist = " + dist);
        float dist = camera.CalculateSFramingDistance(radius);
        camera.transform.position = new Vector3(center.x, center.y, -dist); 
        //    if (camera.orthographic)
        //        camera.orthographicSize = radius;


    }

    public static void FrameBoard(this Camera camera, Board board)
    {
        camera.transform.position = new Vector3(board.transform.position.x, board.transform.position.y, -board.boardFramingDistance);
    }
    public static void FrameCell(this Camera camera, Cell cell)
    {
        camera.transform.position = new Vector3(cell.transform.position.x, cell.transform.position.y, -cell.parentBoard.cellFramingDistance);
    }

    public static T[] Shift<T>(this T[] zeeArray, int newStartIndex)
    {
        if (newStartIndex > zeeArray.Length)
        {
            throw new Exception("you are trying to shift with a number larger than the total items in the array");
        }
        if (newStartIndex < 0)
        {
            throw new Exception("the array can't be shifted with a negative number");
        }
        var newArray = new T[zeeArray.Length];
        Array.Copy(sourceArray: zeeArray, sourceIndex: newStartIndex, destinationArray: newArray, destinationIndex: 0, length: zeeArray.Length - newStartIndex);
        Array.Copy(sourceArray: zeeArray, sourceIndex: 0, destinationArray: newArray, destinationIndex: zeeArray.Length - newStartIndex, length: newStartIndex);
        return newArray;
    }
}

public static class GameObjectExtenstions
{
    /// <summary>
    /// outputs bound of all children of teh game object
    /// </summary>
    /// <param name="go"> the  parent of objects to calculate bounds for</param>
    /// <returns>returns bounds</returns>
    public static Bounds CalculateBounds(this GameObject go)
    {
        Bounds b = new Bounds(go.transform.position, Vector3.zero);
        UnityEngine.Object[] rList = go.GetComponentsInChildren(typeof(Renderer));
        foreach (Renderer r in rList)
        {
            b.Encapsulate(r.bounds);
        }
        return b;
    }



}
