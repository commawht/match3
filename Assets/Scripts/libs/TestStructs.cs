﻿using System;
using System.Reflection;
using System.Collections;
using UnityEngine;
using UnityEngine.Internal;

namespace MStructs
{
    public struct MPos
    {
        public int x,y;
        public MPos(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public MPos(Vector2 vector2){
            this.x = (int)vector2.x;
            this.y = (int)vector2.y;
        }

        public static MPos operator +(MPos a, MPos b)
        {
            return new MPos(a.x + b.x, a.y + b.y);
        }
        public static bool operator ==(MPos a, MPos b)
        {
            if (a.x == b.x && a.y == b.y)
            {
                return true;
            }
            else return false;
        }
        public static bool operator !=(MPos a, MPos b)
        {
            if (a.x == b.x && a.y == b.y)
            {
                return false;
            }
            else return true;
        }


        //
        public override string ToString()
        {
            //string converted;
            //converted = "("+x+","+y+")";
            return "(" + x + "," + y + ")";
        }

        //public static MPos operator -(MPos a);
        //public static MPos operator -(MPos a, MPos b);
        //public static bool operator !=(MPos lhs, MPos rhs);
        //public static MPos operator *(float d, MPos a);
        //public static MPos operator *(MPos a, float d);
        //public static MPos operator /(MPos a, float d);
        //public static MPos operator +(MPos a, MPos b);
        //public static bool operator ==(MPos lhs, MPos rhs);

    }
}
namespace MClasses
{
    //[Serializable]
    public struct ColorAndWeight // struct to avoid pass by reference in the array
    {
        public Color color { get; set; }
        public float weight { get; set; }


        //public ColorAndWeight()
        //{
        //    this.color = Color.black;
        //    this.weight = 0;
        //}
        public ColorAndWeight(Color col, float wght)
        {
            this.color = col;
            this.weight=wght;
        }
    }
}
