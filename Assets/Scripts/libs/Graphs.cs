﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
/*
 Folowing https://msdn.microsoft.com/en-us/library/ms379574%28v=vs.80%29.aspx
 * 
 */
public class Graph : MonoBehaviour {

    public class Node<T>//Abstract?
    {
        //private member-variables
        private T data;
        private NodeList<T> neighbours = null;

        public Node() { }
        public Node(T data) : this(data, null) { }
        public Node(T data, NodeList<T> neighbours)
        {
            this.data = data;
            this.neighbours = neighbours;
        }

        public T Value
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        protected NodeList<T> Neighbours
        {
            get
            {
                return neighbours;
            }
            set
            {
                neighbours = value;
            }
        }
    } 

    public class NodeList<T> : Collection<Node<T>> {//Abstract?
        public NodeList() : base() { }

        public NodeList(int initialSize)
        {
            //add specified number of items
            for (int i = 0; i < initialSize; i++)
            {
                base.Items.Add(default(Node<T>)); // from objectModel

            }
        }

        public Node<T> FindByValue(T value){
            foreach (Node<T> node in Items){
                if(node.Value.Equals(value)){
                    return node;
                }
            }
            //if we reached here, we didn't find a matching node
            return null;
        }
    }



    //Generic graph defs
    public class GraphNode<T>: Node<T>
    {
        private List<int> costs; //weight from a GraphNode to a specific neighbour

        public GraphNode() : base() { }
        public GraphNode(T value ): base(value){}
        public GraphNode(T value, NodeList<T> neighbours) : base(value, neighbours) { }

        new public NodeList<T> Neighbours
        {
            get
            {
                if (base.Neighbours == null)
                {
                    base.Neighbours = new NodeList<T>();
                }
                return base.Neighbours;
            }
        }
        public List<int> Costs
        {
            get
            {
                if (costs == null)
                {
                    costs = new List<int>();
                }
                return costs;
            }
        }
    }

    //public class Graph<T> //: IEnumerable<T> //must also implement getenumerator... is Ienumerable necessary?
    //{
    //    private NodeList<T> nodeSet;

    //    public Graph() : this(null) {}
    //    public Graph(NodeList<T> nodeSet) {
    //        if (nodeSet == null)
    //        {
    //            this.nodeSet = new NodeList<T>();

    //        }else {
    //            this.nodeSet = nodeSet;
    //        }
    //    }
    //    //add a node to the graph
    //    public void AddNode(GraphNode<T> node)
    //    {
    //        nodeSet.Add(node);
    //    }
    //    public void AddNode(T value)
    //    {
    //        nodeSet.Add(new GraphNode<T>(value));
    //    }
    //    //add edges
    //    public void AddUndirectedEdge(GraphNode<T> from, GraphNode<T> to, int cost)
    //    {
    //        from.Neighbours.Add(to);
    //        from.Costs.Add(cost);

    //        to.Neighbours.Add(from);
    //        to.Costs.Add(cost);
    //    }

    //    public void AddDirectedEdge(GraphNode<T> from, GraphNode<T> to, int cost) {
    //        from.Neighbours.Add(to);
    //        from.Costs.Add(cost);
    //    }

    //    public bool Remove(T value)
    //    {
    //        //first remove the node from the nodeset
    //        GraphNode<T> nodeToRemove = (GraphNode<T>)nodeSet.FindByValue(value); //WTF ? read on syntax and generics
    //        if (nodeToRemove == null)
    //        {
    //            //node wasn't found
    //            return false;
    //        }else {
    //            nodeSet.Remove(nodeToRemove);
    //        }
    //        // enumerate through each node in the nodeSet, removing edges to this node
    //        foreach (GraphNode<T> gnode in nodeSet)
    //        {
    //            int index = gnode.Neighbours.IndexOf(nodeToRemove);
    //            if (index != -1)//WHY ? in what case can  index of neighbours be -1? (Ienumerator is positioned before the first item in the collection?)
    //            {
    //                //remove the reference to the node and asociated cost
    //                gnode.Neighbours.RemoveAt(index);
    //                gnode.Costs.RemoveAt(index);
    //            }
    //        }
    //        return true;        
    //    }

    //    public NodeList<T> Nodes
    //    {
    //        get
    //        {
    //            return nodeSet;
    //        }
    //    }

    //    public int Count
    //    {
    //        get { return nodeSet.Count; }
    //    }


    //}







}
