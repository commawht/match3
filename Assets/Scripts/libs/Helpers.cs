﻿using UnityEngine;
using System;
using System.Collections;
using System.Linq;
using MClasses;

public static class Helpers {

    public static float CalcBoxDiagonal(float width, float depth, float height)
    {
        return Mathf.Sqrt(width * width + depth * depth + height * height);
    }

    public static void ReverseNormals(MeshFilter filter){
        //MeshFilter filter = GetComponent(typeof(MeshFilter)) as MeshFilter;
        if (filter != null)
        {
            Mesh mesh = filter.mesh;

            Vector3[] normals = mesh.normals;
            for (int i = 0; i < normals.Length; i++)
                normals[i] = -normals[i];
            mesh.normals = normals;

            for (int m = 0; m < mesh.subMeshCount; m++)
            {
                int[] triangles = mesh.GetTriangles(m);
                for (int i = 0; i < triangles.Length; i += 3)
                {
                    int temp = triangles[i + 0];
                    triangles[i + 0] = triangles[i + 1];
                    triangles[i + 1] = temp;
                }
                mesh.SetTriangles(triangles, m);
            }
        }
    }

    public static int WeightedRandom(float[] weightedIds)
    {
        //we have to pass through teh array 2 times perhaps there is a better way?

         float totalWeights = 0;
        foreach (float weight in weightedIds)
        {
            totalWeights += weight;
        }
        float proposedNumber = UnityEngine.Random.RandomRange(0f, totalWeights);

        for (int i = 0; i < weightedIds.Length; i++)
        {

            if (proposedNumber < weightedIds[i])
            {
                return i;
            }
            else
            {
                proposedNumber = proposedNumber - weightedIds[i];

            }

        }
        Debug.Log("we have a problem in Aboard.WeightedRandom .. got to the end and no result");
        return -1; //just to make sure we get an error too
    }

    public static int WeightedRandom(ColorAndWeight[] weightedColors)
    {
        //we have to pass through teh array 2 times perhaps there is a better way?
    
        float totalWeights = 0;
        foreach (ColorAndWeight weightedColor in weightedColors)
        {
            totalWeights += weightedColor.weight;

        }
        float proposedNumber = UnityEngine.Random.Range(0f, totalWeights);

        for (int i = 0; i < weightedColors.Length; i++)
        {

            if (proposedNumber < weightedColors[i].weight)
            {
                return i;
            }
            else
            {
                proposedNumber = proposedNumber - weightedColors[i].weight;

            }

        }
        Debug.Log("weighted color len  inside random" + weightedColors.Length);
        Debug.Log("we have a problem in Aboard.WeightedRandom .. got to the end and no result");
        return -1; //just to make sure we get an error too
    }

    public static void Swap<T>(ref T a, ref T b)
    {
        T temp = a;
        a = b;
        b = temp;
    }

    /*
     *cell weights shift 
     */
    public static ColorAndWeight[] CellWeightShift(ColorAndWeight[] cellArray, int importantCellIndex)
    {
        float tempWeight = cellArray[importantCellIndex].weight;
        cellArray[importantCellIndex].weight = cellArray[0].weight;
        cellArray[0].weight = tempWeight;
        //int arrayLen = cellArray.Length;
        //int indexOfTargetWeight; // target index of the weight shift
        //float tempWeight;

        //if (importantCellIndex < 0)
        //{
        //    throw new Exception("the index provided is " + importantCellIndex +" and negative numbers are not alowed .. it is the index of an array duuh");
           
        //}
        //if (importantCellIndex >= arrayLen)
        //{
        //    throw new Exception("the index provided is " + importantCellIndex + " and it is larger than the length of the proveded array");

        //}
        ////shift the weights in the array
        //for (int i = 0; i < cellArray.Length; i++)
        //{

        //    indexOfTargetWeight = (i + importantCellIndex) % arrayLen;

        //    //tempWeight = cellArray[indexOfTargetWeight].weight;

        //    cellArray[indexOfTargetWeight].weight = cellArray[i].weight;
        //    //cellArray[i].weight = tempWeight;
        //    //cellArray[indexOfTargetWeight].weight = -1f;


        //}
        return cellArray;

    }


    /// <summary>
    /// Counts the cells with the same id and outputs an array where the index is the id and the values is the number of occurences
    /// </summary>
    /// <returns></returns>
    public static int[] CellCount(Board board)
    {

        var querry = from Cell cell in board.cellMatrix
                     group cell by cell.myID into groups
                     orderby groups.Key
                     select groups.Count();
        return querry.ToArray();

    }

    /// <summary>
    /// Finds the most frequent ID
    /// </summary>
    /// <returns> Return the ID of the most frequent cell</returns>

    public static int PrevalentID(int[] countedCells)
    {
        int maxValue = 0;
        int maxValueID = 0;
        for (int i = 0; i < countedCells.Length; i++)
        {
            if (maxValue<countedCells[i]){
                maxValue = countedCells[i];
                maxValueID = i;
            }
        }
        return maxValueID;
    }
}
