﻿using UnityEngine;
using System.Collections;

public class GUIMaster : MonoBehaviour {
    GameMaster gameMasterInstance;
	// Use this for initialization
	void Start () {
        gameMasterInstance = GameMaster.gmInstance;

	}

    void OnGUI()
    {
        GUILayout.BeginArea(new Rect(10, 10, Screen.width/2, Screen.height));
        GUILayout.Label("score " + GameMaster.score);
        GUILayout.Label("moves " + GameMaster.moves);
        //display current board composition
        if (GameMaster.curBComp != null)
        {
            GUILayout.Label("Current Board is " + GameMaster.currentBoard);

            GUILayout.Label( "Depth " + GameMaster.currentBoard.boardDepth);

            for (int i = 0; i < GameMaster.curBComp.Length; i++)
            {
                GUILayout.Label("id  " + i + " has " + GameMaster.curBComp[i] );
            }

            for (int i = 0; i < GameMaster.currentBoard.thisBoardsAvailablePieces.Length; i++)
            {
                GUILayout.Label("id  " + i + " has weight" + GameMaster.currentBoard.thisBoardsAvailablePieces[i].weight);
            }

            GUILayout.Label("the prevalent id is " + Helpers.PrevalentID(GameMaster.curBComp));
        }
        GUILayout.EndArea();

    }
}
