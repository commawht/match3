﻿using UnityEngine;
using System;
using System.Collections;
using MClasses;
//using ZeeMatrix;

/// <summary>
/// singleton
/// </summary>

//RESEARCH: toolbox to replace singleton
public class GameMaster : MonoBehaviour {
    //game configurator

    //test

    public AnimationCurve weightsCurve;

    public Transform baseGeometry;
    //public ColorAndWeight colAndWeight;


    //public static int maxDepth = 6;// this should be calculated based on camera clipping and dimensions of boards

    public int currentLvl;


    public ColorAndWeight[] lvlPieces ;


    
    
    //singleton
    private static GameMaster _gmInstance;
    public static GameMaster gmInstance {

        get
        {
            if (_gmInstance == null){
                _gmInstance = GameObject.FindObjectOfType<GameMaster>();
                DontDestroyOnLoad(_gmInstance.gameObject);
            }
            return _gmInstance;
        }
    }


    //[HideInInspector]
    public static int moves = 0;
    public static int score = 0;

    //private Board BaseBoard;    // The first board
    private static Board _currentBoard;

    public static Board currentBoard
    {
        get { return GameMaster._currentBoard; }
        set {
            Debug.Log("new value for current board");
            GameMaster._currentBoard = value;

            //debug
            value.ShowBoardInfo(availablePieces: true);
        }
    }
    //private Color parentColor;

    //gui related stuff
    //current board composition
    public static int[] curBComp; //the composition of the current board


    void Awake()//singleton 
    {
        if (_gmInstance == null)
        {

            _gmInstance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            if (this != _gmInstance)
                Destroy(this.gameObject);
        }

    }
    // Use this for initialization
	void Start () {
        //ColorAndWeight[] tempArray = new ColorAndWeight[]{  new ColorAndWeight(Color.white, 0f),
        //                                                    new ColorAndWeight(Color.white, 1f),
        //                                                    new ColorAndWeight(Color.white, 2f),
        //                                                    new ColorAndWeight(Color.white, 3f),
        //                                                    new ColorAndWeight(Color.white, 4f),
        //                                                    new ColorAndWeight(Color.white, 5f)};
        ////DebugInfos.PrintWeights(tempArray);
        //tempArray = Helpers.CellWeightShift(tempArray, 3);
        //Debug.Log("shifted Array");
        //DebugInfos.PrintWeights(tempArray);
        
        
        InitializeGame();   
        DecideWeights(length: 2, curve: weightsCurve);

  
        NewBoard();
	}

    public void InitializeGame()
    {
        currentLvl = 0;
        lvlPieces = GeneratePieces(lvl: currentLvl, weightscurve: weightsCurve);
        //Debug.Log("lvlpieces " + lvlPieces.Length);
    }
    public void NewBoard()
    {

       GameObject newBoard = new GameObject();
       Board newBoardScript = newBoard.AddComponent<Board>();

       newBoard.transform.localScale = newBoard.transform.localScale * 100; //TODO expose info used to create board
            //intialize TEST board
       newBoardScript.nrOfRows = DecideNrOfRows(newBoardScript.boardDepth);
       newBoardScript.thisBoardsAvailablePieces = DecideBoardPieces(depth: newBoardScript.boardDepth,
                                                                    allPieces: lvlPieces,
                                                                    parentCellIndex: 0);
       newBoard.name = "Board_Depth-" + newBoardScript.boardDepth;

        newBoardScript.Initialize();
        _currentBoard = newBoardScript;
        Camera.main.FrameBoard(_currentBoard);

    }

    public  Board NewBoardInCell(Cell zeeCell){
        Cell zeeCellScript = zeeCell.GetComponent<Cell>();   // cache the script component of the cell
    

            GameObject newBoard = new GameObject();             //empty game object to parent the board to .. necessary?
            Board newBoardScript = newBoard.AddComponent<Board>();
            newBoardScript.nrOfRows = DecideNrOfRows(currentBoard.boardDepth+1);
            newBoardScript.boardDepth = zeeCellScript.parentBoard.boardDepth + 1;
            //newBoardScript.weightsAndColorsofPieces = lvlPieces;
            newBoardScript.thisBoardsAvailablePieces = DecideBoardPieces(depth: currentBoard.boardDepth,
                                                                allPieces: lvlPieces,
                                                                parentCellIndex: zeeCellScript.myID);
        
            newBoard.transform.position = zeeCell.transform.position;   //center the new board on the position of the cell
            newBoard.transform.localScale = zeeCell.transform.lossyScale *0.9f; // multiplication with 0.9 f so we don't have overlapping faces .. shopuld be exposed as it is dependent on geometry asset
            newBoard.transform.parent = zeeCell.transform;
            newBoard.name = "Board_Depth-" + newBoardScript.boardDepth;



 

            newBoardScript.Initialize(zeeCell);
            
            return newBoardScript;


    }

    private static int DecideNrOfRows(int depth)
    {

        //basic ideea is that the deeper you go the larger the board perhaps a fibonaci sequence?
        return depth + 1;
    
    }

    public static int DecideNrOfColors(int lvl)//how many colors are available to this lvl
    {
        return (6 + lvl) / 2;//RESEARCH try to use a curve
    }

    public static float[] DecideWeights(int length, AnimationCurve curve)
    {
        if (length == 0)
        {
            Debug.Log("weight length is set to 0 .. bailing out");
            return null;
        }
        float[] weights = new float[length];
        float step = 0; //steps at wich to evaluate the curve
        //find the step the evaluate (total animation curve time divided by nr of weights)
        if (curve.length>0){
            step = curve[curve.length-1].time /length;

        }
        else
        {
            Debug.Log("the curve used to drive the weights is empty");
            return null;

        }
        for (int i = 0; i<length; i++){
            weights[i] = curve.Evaluate(i * step);
            //Debug.Log("weight at " + i + " is " + weights[i]);
        }
       
        return weights;
    }

    private static ColorAndWeight[] GeneratePieces(int lvl, AnimationCurve weightscurve)
    {
        int nrOfColors = DecideNrOfColors(lvl);
        ColorAndWeight[] colWeight = new ColorAndWeight[nrOfColors];
        //ColorAndWeight coltest;
        //coltest = colWeight[0];
        //Debug.Log("coltest weight" + coltest.weight);
        float step; //step to evaluate the curve
        HSBColor tempHSBColor;
        if (weightscurve.length >0)
        {
            step = weightscurve[weightscurve.length - 1].time / nrOfColors;
        }
        else
        {
            Debug.Log("the curve used to drive the weights is empty");
            return null;
        }
        for (int i = 0; i < colWeight.Length; i++)
        {
            tempHSBColor = new HSBColor(    h: (1f/colWeight.Length) * i ,
                                            s: 1f,
                                            b: 1f,
                                            a: 1f);
 
            //Debug.Log("col is " + (1f / colWeight.Length) * i);
            colWeight[i] = new ColorAndWeight(tempHSBColor.ToColor(), weightscurve.Evaluate(i * step));
            colWeight[i].weight = weightscurve.Evaluate(i * step);
            //Debug.Log("generating color and weights, index is " + i + " weight is " + colWeight[i].weight);
        }

            return colWeight;
    }

    private static ColorAndWeight[] DecideBoardPieces(int depth  , ColorAndWeight[] allPieces, int parentCellIndex )
    {
        //the deeper you go the more pieces are available to generate a board from
        int nrOfPices = depth + 1;
        ColorAndWeight[] availabePieces = new ColorAndWeight[nrOfPices];

        if (allPieces.Length > nrOfPices)
        {
            Array.Copy(allPieces, availabePieces, nrOfPices);
        }
        else 
        {
            Debug.Log("this board wants to have more pieces but there is a limited number available");
           
            availabePieces = (ColorAndWeight[])allPieces.Clone(); // pass by value or reference
        }

            // shift the weights so the parent id has largest chance of geting instantiated
        Debug.Log("all pieces weights");

        Helpers.CellWeightShift(availabePieces, parentCellIndex);
        DebugInfos.PrintWeights(allPieces);
        return availabePieces;
    }

    void LateUpdate()
    {
        if (currentBoard != null)
        {   // RESEARCH count the cells in the current board perhaps counting each frame not so great
            curBComp = Helpers.CellCount(currentBoard); 
            //the id with largest number of occurences

            if (currentBoard.parentCell!=null && currentBoard.parentCell.myID != Helpers.PrevalentID(curBComp))
            {
                currentBoard.parentCell.myID = Helpers.PrevalentID(GameMaster.curBComp);//TODO find another way as we are calling the function PrevalentID twice
            }
        }
        else Debug.Log("no current board? WTF?");

    }


}
